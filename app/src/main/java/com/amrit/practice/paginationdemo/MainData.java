package com.amrit.practice.paginationdemo;

public class MainData {

    private String stockItemId, storeId, name, priceUnit, lastUpdateTime;
    private String purchasePrice;
    private String sellingPrice;
    private String mrp;
    private String availableCount, lowCountLimit;
    private String isItemCountLow;

    public MainData(String stockItemId, String storeId, String name, String priceUnit,
                    String lastUpdateTime, String purchasePrice, String sellingPrice, String mrp, String availableCount,
                    String lowCountLimit, String isItemCountLow) {
        this.stockItemId = stockItemId;
        this.storeId = storeId;
        this.name = name;
        this.priceUnit = priceUnit;
        this.lastUpdateTime = lastUpdateTime;
        this.purchasePrice = purchasePrice;
        this.sellingPrice = sellingPrice;
        this.mrp = mrp;
        this.availableCount = availableCount;
        this.lowCountLimit = lowCountLimit;
        this.isItemCountLow = isItemCountLow;
    }

    public String getStockItemId() {
        return stockItemId;
    }

    public void setStockItemId(String stockItemId) {
        this.stockItemId = stockItemId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(String priceUnit) {
        this.priceUnit = priceUnit;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(String availableCount) {
        this.availableCount = availableCount;
    }

    public String getLowCountLimit() {
        return lowCountLimit;
    }

    public void setLowCountLimit(String lowCountLimit) {
        this.lowCountLimit = lowCountLimit;
    }

    public String getIsItemCountLow() {
        return isItemCountLow;
    }

    public void setIsItemCountLow(String isItemCountLow) {
        this.isItemCountLow = isItemCountLow;
    }
}
