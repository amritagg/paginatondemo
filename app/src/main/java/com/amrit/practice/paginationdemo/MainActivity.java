package com.amrit.practice.paginationdemo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    NestedScrollView nestedScrollView;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ArrayList<MainData> dataArrayList;
    MainAdapter adapter;
    String BASE_URL = "http://stock.digitalregister.in:8080/api/v4/stockItem/getPaginatedItemsList/";
    int page = 0;
    boolean next = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataArrayList = new ArrayList<>();
        nestedScrollView = findViewById(R.id.scroll_view);
        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress_bar);

        adapter = new MainAdapter(dataArrayList);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        try {
            getData(page);
        } catch (JSONException e) {
            Log.e("MAIN", "Error in json");
        }

        nestedScrollView.setOnScrollChangeListener(
                (NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if(scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                try {
                    getData(page++);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getData(int page) throws JSONException {

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject("{\"storeId\":\"9a917180-f9b7-465f-b1d6-dd1b4cb5a9e6\",\n" +
                "\"searchString\": \"\",\"categoryId\": \"ALL\",\"offset\": " + page + "," +
                "\"isLowCountItems\": false}");
        jsonArray.put(jsonObject);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, BASE_URL, jsonArray,
                array -> {
                    try {
                        if(array.length() == 0){
                            next = false;
                            progressBar.setVisibility(View.GONE);
                            return;
                        }
                        for(int i = 0; i < array.length(); i++){
                            JSONObject jsonOb = array.getJSONObject(i);
                            String stockItemId = jsonOb.getString("stockItemId");
                            String storeId = jsonOb.getString("storeId");
                            String name = jsonOb.getString("name");
                            String priceUnit = jsonOb.getString("priceUnit");
                            String lastUpdateTime = jsonOb.getString("lastUpdatedTime");
                            String purchasePrice = jsonOb.getString("purchasePrice");
                            String sellingPrice = jsonOb.getString("sellingPrice");
                            String mrp = jsonOb.getString("mrp");
                            String availableCount = jsonOb.getString("availableCount");
                            String lowCountLimit = jsonOb.getString("lowCountLimit");
                            String isItemCountLow = jsonOb.getString("isItemCountLow");

                            MainData mainData = new MainData(stockItemId, storeId, name, priceUnit,
                                    lastUpdateTime, purchasePrice, sellingPrice, mrp,
                                    availableCount, lowCountLimit, isItemCountLow);


                            dataArrayList.add(mainData);
                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        Log.e("AMIN", "this is error " + e);
                    }
                }, error -> Log.e("Main", "error " + error)) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonArrayRequest);

    }

}