package com.amrit.practice.paginationdemo;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private final ArrayList<MainData> dataArrayList;

    public MainAdapter(ArrayList<MainData> dataArrayList) {
        this.dataArrayList = dataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MainData data = dataArrayList.get(position);

        Log.e("MAIN", "this is called");
        holder.name.setText(data.getName());
        holder.sellingPrice.setText("\u20B9 " + data.getSellingPrice());
        holder.stockPrice.setText("\u20B9 " + data.getPurchasePrice());
        holder.remainingStock.setText(data.getAvailableCount() + " " + data.getPriceUnit());
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, sellingPrice, stockPrice, remainingStock;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            sellingPrice = itemView.findViewById(R.id.selling_price_value);
            stockPrice = itemView.findViewById(R.id.stock_price_value);
            remainingStock = itemView.findViewById(R.id.remaining_stock_value);
        }
    }
}
